class Dog {
  bark() {
    return 'Bark';
  }

  jump() {
    return 'Jump';
  }

  eat() {
    return 'Eat';
  }

  sleep() {
    return 'Sleep';
  }
}

module.exports = { Dog }
